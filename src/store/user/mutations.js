/*
export function someMutation (state) {
}
*/

export function setUserInfo(state, val) {
  state.userInfo = val;
}

export function removeUserInfo(state) {
  state.userInfo = "";
}
