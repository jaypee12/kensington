/*
export function someGetter (state) {
}
*/

export function getFullName(state) {
  return state.userInfo.fname + " " + state.userInfo.lname;
}

export function getUserId(state) {
  return state.userInfo.id;
}

export function getUserEmail(state) {
  return state.userInfo.email;
}

export function getUserInfo(state) {
  return {
    id: state.userInfo.id,
    fname: state.userInfo.fname,
    lname: state.userInfo.lname,
    tel_number: state.userInfo.tel_number,
    mob_number: state.userInfo.mob_number,
    address_1: state.userInfo.address_1,
    address_2: state.userInfo.address_2,
    address_3: state.userInfo.address_3,
    town: state.userInfo.town,
    default_currency: state.userInfo.default_currency
  };
}

export function getAvatar(state) {
  return (
    "https://ui-avatars.com/api/?background=434842&color=fff&&name=" +
    state.userInfo.fname +
    "%20" +
    state.userInfo.lname +
    ""
  );
}

export function isLogedIn(state) {
  if (state.userInfo != "") {
    return true;
  }
  return false;
}

export function isEmailEnabled(state) {
  return state.userInfo.is_email_enabled;
}

export function isWhatsAppEnabled(state) {
  return state.userInfo.is_whatsapp_enabled;
}
