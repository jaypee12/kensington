/*
export function someAction (context) {
}
*/

export function login({ commit }, value) {
  commit("setUserInfo", value.userInfo);
}

export function logout({ commit }) {
  commit("removeUserInfo");
}
