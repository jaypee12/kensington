import Vue from "vue";
import axios from "axios";
import moment from "moment";
import CryptoJS from "crypto-js";

var apikey = "yz8pgGNNdwEQVHvGweMJpFA23Abbn6Fr5bzMJzBn";
var secretkey = "uxG49mS45XJvLSxwHkDVp9mrrTfQnNsf";
var nonce = moment()
  .utc(moment())
  .unix();

var signature = nonce + apikey + secretkey;
var hash = CryptoJS.HmacSHA256(signature, secretkey);

axios.defaults.headers.common["Content-Type"] =
  "application/x-www-form-urlencoded";
axios.defaults.headers.common["API_KEY"] = apikey;
axios.defaults.headers.common["ACCESS_NONCE"] = nonce;
axios.defaults.headers.common["Access-Control-Allow-Origin"] = "*";
axios.defaults.headers.common["API_SIGNATURE"] = hash.toString();

Vue.prototype.$axios = axios;
