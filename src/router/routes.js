import {
  Store
} from "../store/index.js";
const routes = [{
    path: "/",
    component: () => import("layouts/HomeLayout.vue"),
    meta: {
      requiresGuest: true
    },
    children: [{
        path: "",
        component: () => import("pages/Index.vue")
      },
      {
        path: "login",
        component: () => import("pages/Authentication/Login.vue")
      }
    ]
  },
  {
    path: "/register",
    component: () => import("layouts/RegisterLayout.vue"),
    meta: {
      requiresGuest: true
    },
    children: [{
      path: "",
      component: () => import("pages/Authentication/Register.vue")
    }]
  },
  {
    path: "/user",
    component: () => import("layouts/MainLayout.vue"),
    meta: {
      requiresAuth: true
    },
    children: [{
        path: "",
        component: () => import("pages/user/Index.vue")
      },
      {
        path: "account",
        meta: {
          isProfile: true
        },
        component: () => import("pages/user/account/Index.vue"),
        children: [{
            path: "",
            meta: {
              isProfile: true
            },
            component: () => import("pages/user/account/Profile.vue")
          },
          {
            path: "bank",
            meta: {
              isProfile: true
            },
            component: () => import("pages/user/account/BankAccount.vue")
          },
          {
            path: "personaldocuments",
            meta: {
              isProfile: true
            },
            component: () => import("pages/user/account/PersonalDocuments.vue")
          },
          {
            path: "taxresidency",
            meta: {
              isProfile: true
            },
            component: () => import("pages/user/account/TaxResidency.vue")
          },
          {
            path: "statementPeriod",
            meta: {
              isProfile: true
            },
            component: () => import("pages/user/account/StatementPeriod.vue")
          },
          {
            path: "securitySettings",
            meta: {
              isProfile: true
            },
            component: () =>
              import("pages/user/account/SecuritySettings.vue")
          }
        ]
      },
      {
        path: "history",
        component: () => import("pages/user/history.vue")
      },
      {
        path: "trade",
        name: "trade",
        component: () => import("pages/user/account/Trade.vue")
      },
      {
        path: "file",
        component: () => import("pages/user/Statement.vue")
      }
    ]
  },
  {
    path: "/notification",
    meta: {
      requiresAuth: true
    },
    component: () => import("layouts/NotificationLayout.vue"),
    meta: {
      requiresAuth: true
    },
    children: [{
      path: "",
      component: () => import("pages/notification/Index.vue")
    }]
  },
  {
    path: "/redeem",
    meta: {
      requiresAuth: true
    },
    component: () => import("layouts/TradeLayout.vue"),
    children: [{
      path: "",
      component: () => import("pages/onRedeem/Index.vue")
    }]
  },
  {
    path: "/buy",
    component: () => import("layouts/TradeLayout.vue"),
    meta: {
      requiresAuth: true
    },
    children: [{
      path: "",
      component: () => import("pages/onPurchase/Index.vue")
    }]
  },
  {
    path: "/redemption/:id",
    meta: {
      requiresAuth: true
    },
    component: () => import("layouts/RedemptionLayout.vue"),
    meta: {
      requiresAuth: true
    },
    props: true,
    children: [{
      path: "",
      component: () => import("pages/redemption/Index.vue")
    }]
  },
  {
    path: "/purchase/:id",
    meta: {
      requiresAuth: true
    },
    component: () => import("layouts/PurchaseLayout.vue"),
    meta: {
      requiresAuth: true
    },
    props: true,
    children: [{
      path: "",
      component: () => import("pages/purchase/Index.vue")
    }]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
